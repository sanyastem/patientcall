﻿using System;
namespace DomainViewModal
{
    public class PatientLogin
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
