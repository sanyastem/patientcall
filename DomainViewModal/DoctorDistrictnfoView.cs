﻿using System;
namespace DomainViewModal
{
    public class DoctorPDistrictInfoView
    {
        public string District { get; set; }
        public string FIO { get; set; }
        public string Address { get; set; }
    }
}
