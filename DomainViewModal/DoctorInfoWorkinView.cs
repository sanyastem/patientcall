﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainViewModal
{
    public class DoctorInfoWorkinView
    {
        public string District { get; set; }
        public string FIO { get; set; }
        public string Position { get; set; }
        public string Time { get; set; }
    }
}
