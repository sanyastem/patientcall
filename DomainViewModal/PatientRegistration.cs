﻿using System;
namespace DomainViewModal
{
    public class PatientRegistration
    {
        public string Passport { get; set; }
        public string FIO { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
