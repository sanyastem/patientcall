﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using BusinessLayer.Doctor;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PatientCall.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperatorController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetDoctors(string LName = null)
        {
            try
            {
                return Ok(new ReadDoctorsWorkingHours().Read());
            }
            catch (Exception e)
            {
                throw;
            }
        }
        [HttpGet("GetDoctorDistrict")]
        public IActionResult GetDoctorDistrict()
        {
            try
            {
                return Ok(new ReadDoctorsDistrict().Read());
            }
            catch 
            {
                throw;
            }
        }
    }
}