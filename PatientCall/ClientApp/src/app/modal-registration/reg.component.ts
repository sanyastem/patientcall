import { Component,OnInit } from '@angular/core';
import { PatienRegistration } from '../ViewModal/PatientRegistration';
import { PatientService } from '../Service/PatientLogic';

@Component({
  selector: 'modal-reg',
  styleUrls: ['./reg.component.css'],
  templateUrl: './reg.component.html',
  providers: [PatientService]
})
export class RegModalComponent {
  isHiddenReg = true;
  public passport?: string;
  public fio?: string;
  public tel?: string;
  public email?: string;
  public address?: string;
  public password?: string;
  public confirmPassword?: string;
  error: string;
  constructor(private patientService: PatientService) { }
  viewModalRegIsHidden(): void {
    this.isHiddenReg = this.isHiddenReg ? false : true;
  }
  savePatientReg(): void {
    if (this.password != this.confirmPassword) {
      this.error = "Пароль не совпадает";
    }
    else {
      this.error = "";
      let patient = new PatienRegistration(this.passport, this.fio, this.tel, this.email, this.address, this.password);
      this.passport = null;
      this.fio = null;
      this.tel = null;
      this.email = null;
      this.address = null;
      this.passport = null;
      var a = this.patientService.Registration(patient).subscribe();
      console.log(a);
      this.viewModalRegIsHidden();
    }
    
  }
}
