import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../Service/DoctorLogic';
import { Doctor } from '../ViewModal/Doctor';

@Component({
  selector: 'submenu-timetable',
  styleUrls: ['./timetable.component.css'],
  templateUrl: './timetable.component.html',
  providers: [DoctorService]
})
export class TimeTableComponent implements OnInit{
  doctors: Doctor[] = [];
  doctor: Doctor = new Doctor();
  constructor(private dataService: DoctorService) { }

  ngOnInit() {
    this.loadDoctors();    
  }
  loadDoctors() {
    this.dataService.getDoctors().subscribe((data: Doctor) => this.doctors.push(data))
  }
}
