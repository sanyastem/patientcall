import { Component } from '@angular/core';
import {PatientService} from '../Service/PatientLogic';
import {PatientLogin} from '../ViewModal/PatientLogin';

@Component({
  selector: 'modal-login',
  styleUrls: ['./login.component.css'],
  templateUrl: './login.component.html',
  providers:[PatientService]
})
export class LoginModalComponent  {

  isHiddenLogin = true;
  public login?:string;
  public password?:string;

  constructor(private patientService: PatientService) { }

  viewModalLoginIsHidden(): void {
    this.isHiddenLogin = this.isHiddenLogin ? false : true;
  }

  savePatientLogin(): void {
    let patient = new PatientLogin(this.login,this.password);
    this.password = null;
    this.login = null;
  var a = this.patientService.Login(patient).subscribe();
    console.log(a);
    this.viewModalLoginIsHidden();
  }
}
