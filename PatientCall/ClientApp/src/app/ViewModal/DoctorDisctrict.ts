export class DoctorDistrict {
  constructor(
    public district?: string,
    public fio?: string,
    public address?: string,
  ) { }
}
