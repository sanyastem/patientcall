export class Doctor {
  constructor(
    public district?: string,
    public fio?: string,
    public position?: string,
    public time?: string,
    ) { }
}
