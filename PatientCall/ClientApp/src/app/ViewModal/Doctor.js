"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Doctor = /** @class */ (function () {
    function Doctor(ID_Doctor, ID_Position, LastName, MidleName, FirstName, Phone, Email, IsDeleted, ID_District) {
        this.ID_Doctor = ID_Doctor;
        this.ID_Position = ID_Position;
        this.LastName = LastName;
        this.MidleName = MidleName;
        this.FirstName = FirstName;
        this.Phone = Phone;
        this.Email = Email;
        this.IsDeleted = IsDeleted;
        this.ID_District = ID_District;
    }
    return Doctor;
}());
exports.Doctor = Doctor;
//# sourceMappingURL=Doctor.js.map