export class PatienRegistration {
  constructor(
    public passport?: string,
    public fio?: string,
    public tel?: string,
    public email?: string,
    public address?: string,
    public password?: string
  ) { }
}