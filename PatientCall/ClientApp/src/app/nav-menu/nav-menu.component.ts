import { Component, ViewChild } from '@angular/core';
import { RegModalComponent } from '../modal-registration/reg.component';
import { LoginModalComponent } from '../modal-login/login.component';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  @ViewChild(RegModalComponent)
  private childReg: RegModalComponent;
  @ViewChild(LoginModalComponent)
  private childLogin: LoginModalComponent;

  isExpanded = false;

  viewModalReg(): void {
    this.childReg.viewModalRegIsHidden();
  }
  viewModalLogin(): void {

    this.childLogin.viewModalLoginIsHidden();
  }
  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
