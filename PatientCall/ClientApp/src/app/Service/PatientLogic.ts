import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PatienRegistration } from '../ViewModal/PatientRegistration';
import { PatientLogin } from '../ViewModal/PatientLogin';
import { Observable } from 'rxjs';

@Injectable()
export class PatientService {
  private urlRegistration = "/api/Account/Registration";
  private urlLogin = "/api/Account/Login";
  constructor(private http: HttpClient) {
  }
  Registration(p: PatienRegistration): Observable<any>  {
    return this.http.post(this.urlRegistration, p);
  }
  Login(p: PatientLogin): Observable<any>  {
    return this.http.post(this.urlLogin, p);
  }
}
