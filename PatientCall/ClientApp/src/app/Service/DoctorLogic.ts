import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DoctorService {
  private url = "/api/Operator";
  private urlDistrict = "/api/Operator/GetDoctorDistrict";
  constructor(private http: HttpClient) {
  }
  getDoctors() {
    return this.http.get(this.url);
  }
  getDoctorDistrict() {
    return this.http.get(this.urlDistrict);
  }
}
