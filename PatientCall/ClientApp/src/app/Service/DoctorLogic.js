"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DoctorService = /** @class */ (function () {
    function DoctorService(http) {
        this.http = http;
        this.url = "/api/Operator/GetDoctors";
    }
    return DoctorService;
}());
exports.DoctorService = DoctorService;
//# sourceMappingURL=DoctorLogic.js.map