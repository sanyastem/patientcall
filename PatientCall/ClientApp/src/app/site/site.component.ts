import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../Service/DoctorLogic';
import { DoctorDistrict } from '../ViewModal/DoctorDisctrict';
@Component({
  selector: 'submenu-site',
  styleUrls: ['./site.component.css'],
  templateUrl: './site.component.html',
  providers: [DoctorService]
})
export class SiteComponent implements OnInit{
  doctorDistrict: DoctorDistrict = new DoctorDistrict();
  doctorsDistrict: DoctorDistrict[] = [];

  constructor(private dataService: DoctorService) { }

  ngOnInit(): void {
    this.dataService.getDoctorDistrict().subscribe((data: DoctorDistrict) => this.doctorsDistrict.push(data));
    }

}
