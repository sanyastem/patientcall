import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  styleUrls:['./home.component.css'],
  templateUrl: './home.component.html',
})
export class HomeComponent {
  helpIsHidden: boolean = true;
  siteIsHidden: boolean = true;
  timetableIsHidden: boolean = true;
  aboutIsHidden: boolean = true;
  clickHidden(text:string): void {
    this.helpIsHidden = !(this.helpIsHidden && text == 'help');
    this.siteIsHidden = !(this.siteIsHidden && text == 'site');
    this.timetableIsHidden = !(this.timetableIsHidden && text == 'timetable');
    this.aboutIsHidden = !(this.aboutIsHidden && text == 'about');
  }
}
