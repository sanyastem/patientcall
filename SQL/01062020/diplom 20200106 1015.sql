﻿--
-- Скрипт сгенерирован Devart dbForge Studio 2019 for MySQL, Версия 8.2.23.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 1/6/2020 10:15:26 AM
-- Версия сервера: 5.6.37
-- Версия клиента: 4.1
--

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE diplom;

--
-- Удалить таблицу `User`
--
DROP TABLE IF EXISTS User;

--
-- Удалить таблицу `Procedure`
--
DROP TABLE IF EXISTS `Procedure`;

--
-- Удалить таблицу `Visit`
--
DROP TABLE IF EXISTS Visit;

--
-- Удалить таблицу `Doctor`
--
DROP TABLE IF EXISTS Doctor;

--
-- Удалить таблицу `Position`
--
DROP TABLE IF EXISTS `Position`;

--
-- Удалить таблицу `Patient`
--
DROP TABLE IF EXISTS Patient;

--
-- Удалить таблицу `District`
--
DROP TABLE IF EXISTS District;

--
-- Установка базы данных по умолчанию
--
USE diplom;

--
-- Создать таблицу `District`
--
CREATE TABLE District (
  ID_District int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Name varchar(50) DEFAULT NULL,
  Address varchar(255) DEFAULT NULL,
  PRIMARY KEY (ID_District)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `Patient`
--
CREATE TABLE Patient (
  ID_Patient int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID_District int(11) UNSIGNED NOT NULL,
  LastName varchar(110) DEFAULT NULL,
  MidleName varchar(110) DEFAULT NULL,
  FirstName varchar(110) DEFAULT NULL,
  Phone varchar(50) DEFAULT NULL,
  Address varchar(255) DEFAULT NULL,
  IsDeleted int(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (ID_Patient)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 2730,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE Patient
ADD CONSTRAINT FK_Patient_District_ID_District FOREIGN KEY (ID_District)
REFERENCES District (ID_District) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать таблицу `Position`
--
CREATE TABLE `Position` (
  ID_Position int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Name varchar(50) DEFAULT NULL,
  PRIMARY KEY (ID_Position)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `Doctor`
--
CREATE TABLE Doctor (
  ID_Doctor int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID_Position int(11) UNSIGNED NOT NULL,
  LastName varchar(110) DEFAULT NULL,
  MidleName varchar(110) DEFAULT NULL,
  FirstName varchar(110) DEFAULT NULL,
  Phone varchar(255) DEFAULT NULL,
  Email varchar(50) DEFAULT NULL,
  IsDeleted int(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (ID_Doctor)
)
ENGINE = INNODB,
AUTO_INCREMENT = 6,
AVG_ROW_LENGTH = 3276,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE Doctor
ADD CONSTRAINT FK_Doctor_Position_ID_Position FOREIGN KEY (ID_Position)
REFERENCES `Position` (ID_Position) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать таблицу `Visit`
--
CREATE TABLE Visit (
  ID_Visit int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID_Patient int(11) UNSIGNED NOT NULL,
  ID_Doctor int(11) UNSIGNED NOT NULL,
  CreateDate datetime DEFAULT NULL,
  Office varchar(50) DEFAULT NULL,
  IsDeleted int(1) UNSIGNED NOT NULL,
  PRIMARY KEY (ID_Visit)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
AVG_ROW_LENGTH = 16384,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE Visit
ADD CONSTRAINT FK_Visit_Doctor_ID_Doctor FOREIGN KEY (ID_Doctor)
REFERENCES Doctor (ID_Doctor) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE Visit
ADD CONSTRAINT FK_Visit_Patient_ID_Patient FOREIGN KEY (ID_Patient)
REFERENCES Patient (ID_Patient) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать таблицу `Procedure`
--
CREATE TABLE `Procedure` (
  ID_Procedure int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID_Visit int(11) UNSIGNED NOT NULL,
  ID_Doctor int(11) UNSIGNED NOT NULL,
  Name_Procedure varchar(255) DEFAULT NULL,
  Price double UNSIGNED NOT NULL DEFAULT 0,
  Is_Deleted int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (ID_Procedure)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
AVG_ROW_LENGTH = 16384,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE `Procedure`
ADD CONSTRAINT FK_Procedure_Doctor_ID_Doctor FOREIGN KEY (ID_Doctor)
REFERENCES Doctor (ID_Doctor) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE `Procedure`
ADD CONSTRAINT FK_Procedure_Visit_ID_Visit FOREIGN KEY (ID_Visit)
REFERENCES Visit (ID_Visit) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать таблицу `User`
--
CREATE TABLE User (
  ID_User int(11) NOT NULL AUTO_INCREMENT,
  Username varchar(100) DEFAULT NULL,
  Password varchar(255) DEFAULT NULL,
  FirstName varchar(255) DEFAULT NULL,
  LastName varchar(255) DEFAULT NULL,
  Email varchar(50) DEFAULT NULL,
  IsBlocked tinyint(1) DEFAULT NULL,
  PRIMARY KEY (ID_User)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

-- 
-- Вывод данных для таблицы District
--
INSERT INTO District VALUES
(1, 'Первый', 'Суворова'),
(2, 'Второй', 'Девятовка'),
(3, 'Третий', 'Ольшанка');

-- 
-- Вывод данных для таблицы `Position`
--
INSERT INTO `Position` VALUES
(1, 'Диагност'),
(2, 'Хирург'),
(3, 'Терапевт'),
(4, 'ЛОР');

-- 
-- Вывод данных для таблицы Patient
--
INSERT INTO Patient VALUES
(1, 1, 'Стеников', 'Викторович', 'Анатолий', '+375 29 596 85 65', 'Окульная 21 - 45', 0),
(2, 2, 'Эрдмон', 'Валерьевна', 'Анастасия', '+375 33 458 75 85', 'Курчатова 22 - 558', 0),
(3, 3, 'Порохов', 'Антонович', 'Валерий', '+ 375 29 852 41 63', 'Напалеоно Орды 12А кв.149', 0),
(4, 1, 'Боранов', 'Александрович', 'Эдуарт', '+375 25 945 86 72', 'Сакольская 22- 45', 0),
(5, 1, 'Лебедевич', 'Андреевич', 'Денис', '+375 29 654 85 34', 'Суворова д.13 кв.457А', 0),
(6, 3, 'Тапчилко', 'Евгеньевич', 'Анатолий', '52 43 78', 'Огинского дом 4/12, кв 47', 0);

-- 
-- Вывод данных для таблицы Doctor
--
INSERT INTO Doctor VALUES
(1, 3, 'Михалюк', 'Викторович', 'Андрей', '+375 29 789 54 86', 'Sertyp@mail.ru', 0),
(2, 3, 'Саврас', 'Анатольевич', 'Виктор', '+375 29 587 45 96', NULL, 0),
(3, 2, 'Марковский', 'Валерьевич', 'Артур', '+375 33 524 85 78', 'Archi@gmail.ru', 0),
(4, 4, 'Пузин', 'Олегович', 'Степан', '+375 29 645 98 24', NULL, 0),
(5, 1, 'Андрушкевич', 'Адольфовна', 'Виктория', '+375 33 654 75 84', 'HiGitler@mail.ru', 0);

-- 
-- Вывод данных для таблицы Visit
--
INSERT INTO Visit VALUES
(1, 2, 1, '2020-01-06 00:00:00', '35', 0);

-- 
-- Вывод данных для таблицы User
--
-- Таблица diplom.User не содержит данных

-- 
-- Вывод данных для таблицы `Procedure`
--
INSERT INTO `Procedure` VALUES
(1, 1, 4, 'Исследование внутреннего уха', 12, 0);

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;