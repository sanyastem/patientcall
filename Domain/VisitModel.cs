﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class VisitModel
    {
        [Key]
        public int ID_Visit { get; set; }
        public int ID_Patient { get; set; }
        public int ID_Doctor { get; set; }
        public string Office { get; set; }
        public DateTime CreateDate { get; set; }
        public int IsDeleted { get; set; }
    }
}
