﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class PositionModel
    {
        [Key]
        public int ID_Position { get; set; }
        public string Name { get; set; }
    }
}
