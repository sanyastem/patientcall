﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class PatientModel
    {
        [Key]
        public int ID_Patient { get; set; }
        public string LastName { get; set; }
        public int ID_District { get; set; }
        public string MidleName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int IsDeleted { get; set; }
    }
}
