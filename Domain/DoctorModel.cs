﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    [Table("doctor")]
    public class DoctorModel
    {
        [Key]
        public int ID_Doctor { get; set; }
        public int ID_Position { get; set; }
        [ForeignKey("ID_Position")]
        public PositionModel Position { get; set; }
        public string LastName { get; set; }
        public string MidleName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int ID_District { get; set; }
        [ForeignKey("ID_District")]
        public DistrictModel District { get; set; }
        public string TimeWork { get; set; }
        public int IsDeleted { get; set; }
    }
}
