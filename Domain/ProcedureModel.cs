﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class ProcedureModel
    {
        [Key]
        public int ID_Procedure { get; set; }
        public int ID_Visit { get; set; }
        public int ID_Doctor { get; set; }
        public string Name_Procedure { get; set; }
        public string Phone { get; set; }
        public int Is_Deleted { get; set; }
        public double Price { get; set; }
    }
}
