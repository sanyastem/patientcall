﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
   public class DistrictModel
    {
        [Key]
        public int ID_District { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
