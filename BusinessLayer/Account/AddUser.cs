﻿using System;
using DataLayer;
using Domain;
using DomainViewModal;

namespace BusinessLayer.Account
{
    public class AddUser
    {
        PatientRegistration _patient;
        public AddUser(PatientRegistration registration)
        {
            _patient = registration;
        }
        public PatientRegistration Read()
        {
            string[] fio = _patient.FIO.Split(' ');
            UserModel model = new UserModel() {Username = _patient.Passport,Email = _patient.Email, FirstName = fio[0],LastName = fio[1],Password = _patient.Password,Role = "patient"};
            try
            {
                new WorkWithBD().InsertUser(model);
                _patient.Role = model.Role;
                return _patient;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
