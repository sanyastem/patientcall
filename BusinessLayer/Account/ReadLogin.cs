﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer;
using DomainViewModal;

namespace BusinessLayer.Account
{
    public class ReadLogin
    {
        private string _login;
        private string _password;
        public ReadLogin(string Login,string Password)
        {
            this._login = Login;
            this._password = Password;
        }
        public List<PatientLogin> Read()
        {
            return new WorkWithBD().ValidateUser(_login,_password).Select(x=>new PatientLogin() {Login = x.Username,Password = x.Password ,Role = x.Role}).ToList();
        }
    }
}
