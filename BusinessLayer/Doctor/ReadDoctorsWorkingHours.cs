﻿using DataLayer;
using System.Collections.Generic;
using DomainViewModal;
using System.Linq;

namespace BusinessLayer.Doctor
{
    public class ReadDoctorsWorkingHours
    {
        int[] ids;
        public ReadDoctorsWorkingHours(int [] ids = null)
        {
            this.ids = ids;
        }
        public IEnumerable<DoctorInfoWorkinView> Read()
        {
            WorkWithBD bD = new WorkWithBD();
            var doctors = bD.SelectStaff();
            IEnumerable<DoctorInfoWorkinView> doctorsView = (from d in doctors
                                                            select new DoctorInfoWorkinView() {District = d.District.Name,FIO = d.FirstName + " " + d.LastName,Position = d.Position.Name,Time = d.TimeWork }).AsEnumerable();
           return doctorsView;
        }
    }
}
