﻿using System;
using System.Linq;
using System.Collections.Generic;
using DomainViewModal;
using DataLayer;

namespace BusinessLayer.Doctor
{
    public class ReadDoctorsDistrict
    {
            public IEnumerable<DoctorPDistrictInfoView> Read()
            {
            return new WorkWithBD().SelectStaff().Select(x => new DoctorPDistrictInfoView() { Address = x.District.Address, District = x.District.Name, FIO = x.FirstName + " " + x.LastName });
            }
    }
}
