﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Domain;
using System.Configuration;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace DataLayer
{
    public partial class ApplicationContext : DbContext
    {
        public DbSet<UserModel> User { get; set; }
        public DbSet<PatientModel> Patient { get; set; }
        public DbSet<DoctorModel> Doctors { get; set; }
        public DbSet<DistrictModel> District { get; set; }
        public DbSet<PositionModel> Position { get; set; }
        public DbSet<ProcedureModel> Procedure { get; set; }
        public DbSet<VisitModel> Visit { get; set; }

        public ApplicationContext()
        {
           // Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var a = new ConfigurationBuilder().Build().GetConnectionString("DefaultConnection");// .GetSection("DefaultConnection");
                optionsBuilder.UseMySQL("server=192.168.0.104; UserId=root; Password=root; database=diplom;CharSet=utf8;");
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
    public class WorkWithBD
    {
        public void InsertUser(UserModel AddUser)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.User.Add(AddUser);
                db.SaveChanges();
            }
        }
        public void InsertPos(PositionModel AddPosition)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Position.Add(AddPosition);
                db.SaveChanges();
            }
        }
        public void InsertPat(PatientModel AddPatient)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Patient.Add(AddPatient);
                db.SaveChanges();
            }
        }
        public void InsertStaff(DoctorModel AddStaff)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Doctors.Add(AddStaff);
                db.SaveChanges();
            }
        }
        public void InsertDistrict(DistrictModel AddDistrict)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.District.Add(AddDistrict);
                db.SaveChanges();
            }
        }
        public void InsertProcedure(ProcedureModel AddProcedure)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Procedure.Add(AddProcedure);
                db.SaveChanges();
            }
        }
        public void InsertVisit(VisitModel AddVisit)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Visit.Add(AddVisit);
                db.SaveChanges();
            }
        }

        public List<PatientModel> SelectPatient(string LName)
        {
            List<PatientModel> patients;
            using (ApplicationContext db = new ApplicationContext())
            {
                patients = db.Patient.Where(p => EF.Functions.Like(p.LastName, $"{LName}%")).ToList();
            }
            return patients;
        }
        public List<DoctorModel> SelectStaff(string LName = null)
        {
            List<DoctorModel> Staffs;
            using (ApplicationContext db = new ApplicationContext())
            {
                if (LName != null)
                {
                    Staffs = (from staff in db.Doctors
                              where staff.LastName == LName
                              select staff).ToList();
                }
                else
                {
                    Staffs = (from d in db.Doctors
                              join p in db.Position on d.ID_Position equals p.ID_Position
                              join di in db.District on d.ID_District equals di.ID_District
                              select new DoctorModel() { Position = p, District = di, Email = d.Email, FirstName = d.FirstName, ID_Doctor = d.ID_Doctor
                              , LastName = d.LastName, IsDeleted = 0, MidleName = d.MidleName, Phone = d.Phone, TimeWork = d.TimeWork }).ToList();
                }

                return Staffs;
            }
        }
        public List<VisitModel> SelectVisit(DateTime Date)
        {
            List<VisitModel> Visits;
            using (ApplicationContext db = new ApplicationContext())
            {
                Visits = (from Visit in db.Visit
                          where Visit.CreateDate == Date
                          select Visit).ToList();
            }
            return Visits;
        }
        public List<ProcedureModel> SelectProc(int IDVisit)
        {
            List<ProcedureModel> Procedures;
            using (ApplicationContext db = new ApplicationContext())
            {
                Procedures = (from Procedure in db.Procedure
                              where Procedure.ID_Visit == IDVisit
                              select Procedure).ToList();
            }
            return Procedures;
        }
        public List<PositionModel> SelectPosition(int IDPos)
        {
            List<PositionModel> Pos;
            using (ApplicationContext db = new ApplicationContext())
            {
                Pos = (from Position in db.Position
                       where Position.ID_Position == IDPos
                       select Position).ToList();
            }
            return Pos;
        }
        public List<DistrictModel> SelecttDistrict(int IDDist)
        {
            List<DistrictModel> Districts;
            using (ApplicationContext db = new ApplicationContext())
            {
                Districts = (from District in db.District
                             where District.ID_District == IDDist
                             select District).ToList();
            }
            return Districts;
        }
        public List<UserModel> ValidateUser(string login,string password)
        {
            List<UserModel> Users;
            using (ApplicationContext db = new ApplicationContext())
            {
                Users = (from User in db.User
                         where User.Username == login && User.Password == password
                         select User).ToList();
            }
            return Users;
        }
        public List<UserModel> SelecttUser(int IDUser)
        {
            List<UserModel> Users;
            using (ApplicationContext db = new ApplicationContext())
            {
                Users = (from User in db.User
                        where User.ID_User == IDUser
                        select User).ToList();
            }
            return Users;
        }
    }
}
